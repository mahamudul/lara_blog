<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Brian2694\Toastr\Facades\Toastr;

class SubscriberController extends Controller
{
    public function index()
    {
    	$sibscribers = Subscriber::latest()->get();
    	return view('admin.subscriber',compact('sibscribers'));
    }

    public function destroy($subscriber)
    {
    	$subscriber = Subscriber::findOrFail($subscriber);
    	$subscriber->delete();
    	Toastr::success('Subscriber Successfully Delete','Success');
    	return redirect()->back();
    }
}
