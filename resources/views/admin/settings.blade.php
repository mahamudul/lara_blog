@extends('layouts.backend.app')
@section('title','Settings')
@push('css')
@endpush
@section('content')
<div class="container-fluied">
	<!-- Tabs With Icon Title -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
					SETTINGS
					</h2>
				</div>
				<div class="body">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#profile_with_icon_title" data-toggle="tab">
								<i class="material-icons">face</i> UPDATE PROFILE
							</a>
						</li>
						<li role="presentation">
							<a href="#setting_with_icon_title" data-toggle="tab">
								<i class="material-icons">change_history</i>CHANGE PASSWROD
							</a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="profile_with_icon_title">
							<div class="body">
								<form method="POST" action="{{ route('admin.profile.update') }}" enctype="multipart/form-data" class="form-horizontal">
									@csrf
									@method('PUT')
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="name">Name :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" name="name" id="name" class="form-control" value="{{ Auth::user()->name }}" >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="email_address_2">Email Address :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="text" name="email" id="email_address_2" value="{{ Auth::user()->email }}" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="image">Profile Image :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="file" name="image" id="image" >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="email_address_2">About :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<textarea name="about" rows="5" class="form-control">{{ Auth::user()->about }}</textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
											<button type="submit"  class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="setting_with_icon_title">
							<div class="body">
								<form method="POST" action="{{ route('admin.password.update') }}"  class="form-horizontal">
									@csrf
									@method('PUT')
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="old_password">Old Password :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="password" name="old_password" id="old_password" class="form-control" placeholder="Enter your old password" >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="password">New Password :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="password" name="password" id="password" class="form-control" placeholder="Enter your new password" >
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
											<label for="confirm_password">Confirm Password :</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
											<div class="form-group">
												<div class="form-line">
													<input type="password" name="password_confirmation" id="confirm_password" class="form-control" placeholder="Enter your new pawword again" >
												</div>
											</div>
										</div>
									</div>								
									<div class="row clearfix">
										<div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
											<button type="submit"  class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- #END# Tabs With Icon Title -->
</div>
@endsection
@push('js')
@endpush