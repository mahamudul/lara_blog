@extends('layouts.backend.app')

@section('title','Tag')


@push('css')
@endpush

@section('content')
	<div class="container-fluid">
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>EDIT TAG</h2>
	                </div>
	                <div class="body">
	                    <form action="{{ route('admin.category.update',$category->id ) }}" method="POST" enctype="multipart/form-data" >
	                    	@csrf
	                    	@method('PUT')
	                        <div class="form-group form-float">
	                            <div class="form-line">
	                                <input type="text" value="{{$category->name}}" id="name" name="name" class="form-control">
	                                <label class="form-label">Category Name</label>
	                            </div>
	                        </div>
							<div class="form-group form-float">
	                            <div class="form-line">
	                                <input type="file" id="name" name="image" class="form-control">
	                                <label class="form-label">Category Image</label>
	                            </div>
	                        </div>
	                        <br>
	                        <a href="{{ route('admin.category.index')}}" type="button" class="btn btn-danger m-t-15 waves-effect">BACK</a>
	                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection

@push('js')
@endpush